# Study of the processor and memory power consumption of coupled sparse/dense solvers

[![pipeline status](https://gitlab.inria.fr/thesis-mfelsoci/slides/sbac-pad/badges/master/pipeline.svg)](https://gitlab.inria.fr/thesis-mfelsoci/slides/sbac-pad/-/commits/master)

In the aeronautical industry, aeroacoustics is used to model the propagation of
acoustic waves in air flows enveloping an aircraft in flight. This for instance
allows one to simulate the noise produced at ground level by an aircraft during
the takeoff and landing phases, in order to validate that the regulatory
environmental standards are met. Unlike most other complex physics simulations,
the method resorts to solving coupled sparse/dense systems. In a previous study,
we proposed two classes of algorithms for solving such large systems on a
relatively small workstation (one or a few multicore nodes) based on compression
techniques. The objective of this paper is to assess whether the positive impact
of the proposed algorithms on time to solution and memory usage translates to
the energy consumption as well. Because of the nature of the problem, coupling
dense and sparse matrices, and the underlying solutions methods, including
dense, sparse direct and compression steps, this yields an interesting processor
and memory power profile which we aim to analyze in details.

[Slides for SBAC-PAD 2022](https://thesis-mfelsoci.gitlabpages.inria.fr/slides/sbac-pad/sbac-pad.pdf)

[Event](https://project.inria.fr/sbac2022/)
