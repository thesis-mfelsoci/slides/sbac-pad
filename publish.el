(require 'org) ;; Org mode support
(require 'ox-publish) ;; publishing functions
(require 'ox-latex) ;; LaTeX publishing functions
(require 'org-ref) ;; bibliography support

;; Disable Babel code evaluation.
(setq org-export-babel-evaluate nil)

;; Load style presets.
(load-file "styles/styles.el")

;; Disable usage of 'minted' package. We do not have any source code listing in
;; the exported document.
(setq org-latex-packages-alist '())

;; Configure HTML website and PDF document publishing.
(setq org-publish-project-alist
      (list
       (list "sbac-pad"
             :base-directory "."
             :base-extension "org"
             :exclude ".*"
             :include ["sbac-pad.org"]
             :publishing-function '(org-beamer-publish-to-pdf-verbose)
             :publishing-directory "./public")))

(provide 'publish)
